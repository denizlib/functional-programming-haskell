dayOfWeek y m d = z
    where
    z = ((z2+5) `mod` 7) + 1         --Modify the result to make it easily understandable. 1. Monday 2. Tuesday etc.
    z2 = (d + t1 + k + t2 + t3 + 5 * j) `mod` 7
    j = floor(fromIntegral y / 100)
    k = y2 `mod` 100
    t1 = floor(fromIntegral(13 * (m2 + 1)) / 5.0)
    t2 = floor(fromIntegral j /4)
    t3 = floor(fromIntegral k /4)
    m2 =                            --to fix the issue caused when the month is january or february
        if m<=2
            then m + 12
            else m
    y2 =                            --to fix the issue caused when the month is january or february
        if m<=2
            then y - 1
            else y
			
			
sundays1 :: Integer -> Integer -> Integer
sundays1 start end = sundays' start 1
    where
    sundays' :: Integer -> Integer -> Integer
    sundays' y m
        | y > end = 0
        | otherwise = if dayOfWeek y m 1 == 7 then rest + 1 else rest --sunday is 7
        where
        nextY = y + floor(fromIntegral m / 12)  -- if m == 12, y += 1
        nextM = (m `mod` 12) + 1
        rest = sundays' nextY nextM
		
tailsundays1 :: Integer -> Integer -> Integer
tailsundays1 start end = sundays' start 1 0
    where
    sundays' :: Integer -> Integer -> Integer -> Integer
    sundays' y m rest
        | y > end = rest
        | otherwise = if dayOfWeek y m 1 == 7 then sundays' nextY nextM (rest + 1) else sundays' nextY nextM rest --sunday is 7
        where
        nextY = y + floor(fromIntegral m / 12)  -- if m == 12, y += 1
        nextM = (m `mod` 12) + 1
			
		
leap y = y `mod` 400 == 0 || y `mod` 4 == 0 && y `mod` 100 /= 0
	--where
	--return =
		--if y `mod` 400 == 0 || y `mod` 4 == 0 && y `mod` 100 /= 0
		--	then True
		--	else False
				
				
daysInMonth y m = return
	where
	return =
		if m == 2
			then if leap y
				then 29
				else 28
			else if m == 4 || m == 6 || m == 9 || m == 11
				then 30
				else 31

sundays2 :: Integer -> Integer -> Integer
sundays2 start end = sundays' start 1 0 ((dayOfWeek start 1 1) + 1)
    where
    sundays' :: Integer -> Integer -> Integer -> Integer -> Integer
    sundays' y m rest weekday 
        | y > end = rest
        | otherwise = if (weekday' `mod` 7) == 0 then sundays' nextY nextM (rest + 1) weekday' else sundays' nextY nextM rest weekday' --sunday is 7
        where
        weekday' = weekday + 1
        days = daysInMonth y m
        nextY = y + floor(fromIntegral m / 12)
        nextM = (m `mod` 12) + 1	

		
{-
Question 5:
Every 400 years, there are 97 leap years. 400/100 - 3. 
400*365 + 97 
146097 / 7 = 20871
There are exactly 20871 weeks every 400 years. Therefore there are equal amounts of mondays, tuesdays etc.
Assuming you choose a day completely random, all days have a 1/7 chance.


-}