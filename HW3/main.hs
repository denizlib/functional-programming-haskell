import qualified Data.Map as Map
import Data.Maybe as Maybe
import Data.Char as Char
import System.Environment
import System.Exit
import Data.Tuple

--function for insertion that increments the value
incrementValue :: Char -> Int -> Int -> Int
incrementValue key new_value old_value = 1 + old_value


--function for union that combines two values
sumValues :: Char -> Int -> Int -> Int
sumValues key left_value right_value = right_value + left_value


--wordCharCounts' is a local function to handle recursion. The function iterates over a string, namely [Char], starting from first char with
--the initial input 0. When n reaches the end of chararr, the function returns an empty Map. For each char, Map.insertWithKey is called
--along with an already existing map that the function intends to insert into. This is where the recursion comes in. insert function calls
--the next char until it reaches the tail
--An example call is:
--wordCharCounts "example"
--it only takes a single word
wordCharCounts :: [Char] -> Map.Map Char Int
wordCharCounts chararr = wordCharCounts' chararr 0
  where
  wordCharCounts' :: [Char] -> Int -> Map.Map Char Int
  wordCharCounts' chararr n
    | n == length chararr = Map.empty
    | otherwise = Map.insertWithKey incrementValue (toLower (chararr !! n)) 1 (wordCharCounts' chararr (n+1))



--sentenceCharCounts' is a local function to handle recursion. firstly, words seperates a string by words and returns a string array
--Then for each word, wordCharCounts is called and it's union'd with the next word. This is where recursive comes in. When it reaches the tail,
--it's union'd with an empyt Map. unionWithKey is similar to insertWithKey in the sense that if a key already exists, it sums their values.
--An example call is:
--sentenceCharCounts "this is an example"
--and it returns its character count Map
sentenceCharCounts :: String -> Map.Map Char Int
sentenceCharCounts strarr = sentenceCharCounts' wordarr 0
  where
  wordarr = words strarr
  sentenceCharCounts' :: [String] -> Int -> Map.Map Char Int
  sentenceCharCounts' wordarr n
    | n < length wordarr =  Map.unionWithKey sumValues (wordCharCounts _word) (sentenceCharCounts' wordarr (n+1))
    | otherwise = Map.empty
      where
      _word = wordarr !! n


dictCharCounts :: [String] -> Map.Map String [(Char, Int)]
dictCharCounts strarr = dictCharCounts' strarr 0
  where
  dictCharCounts' :: [String] -> Int -> Map.Map String [(Char, Int)]
  dictCharCounts' strarr n
    | n == length strarr = Map.empty
    | otherwise = Map.insert (strarr !! n) (Map.toList (wordCharCounts (strarr !! n))) (dictCharCounts' strarr (n+1))


dictWordsByCharCounts :: Map.Map String [(Char, Int)] -> Map.Map [(Char, Int)] [String]
dictWordsByCharCounts dictmap = Map.fromListWith (++) (concatMap (\(k, vs) -> [(vs, [k])]) (Map.toList dictmap))

wordAnagrams :: String -> Map.Map [(Char, Int)] [String] -> Maybe [String]
wordAnagrams word dictmap = Map.lookup (Map.toList (wordCharCounts word)) dictmap


main = do
  args <- getArgs
  text <- readFile "words.txt"
  let kelim = words text
  let dictarr = dictWordsByCharCounts (dictCharCounts kelim)
  let input = args !! 0
  print $ (wordAnagrams input dictarr)
