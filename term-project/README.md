## Important Explanation for Insert Function

Insert function works fine, but I couldn't manage to keep a trie as "global variable" while in a forever $ do loop. So, I had to update words.txt every time a new word is added to the trie.

There is a commented out main function that shows insert function works properly.