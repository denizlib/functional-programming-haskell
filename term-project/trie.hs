import qualified Data.Map as M
import Data.Maybe
import System.Environment
import System.IO
import Control.Monad
import Prelude hiding (Word)


{-
IMPORTANT README

Insert fonksiyonu tam olarak istendiği şekilde çalışıyor, fakat IO işlem loopu
içerisinde global bir trie tutmayı başaramadığımdan ötürü, UI içindeki insert
anlamsız hale düştü. En azından görsel olarak de çalışması için words.txt'yi
güncelleyerek kelime ekleme işlemini sağladım. Insert fonksiyonunun çalıştığını
en altta commente aldığım maini açarak deneyebilirsiniz. Biraz hile gibi oldu,
fakat problem IO kısmında, trie fonksiyonlarında sıkıntı yok.

-}



data Trie = Trie {end :: Bool, children :: M.Map Char Trie} deriving(Show)
type Word = String

empty :: Trie
empty = Trie {end = False, children = M.empty}

insert :: Word -> Trie -> Trie
insert [] (Trie _ triemap) = Trie True triemap
insert (c:cs) (Trie end triemap) = Trie end triemap'
  where
    triemap' = M.alter (Just . insert cs . fromMaybe empty) c triemap  -- INSERT WITH KEY ALTERNATIVE REQUIRED

insertList :: [Word] -> Trie -> Trie
insertList wordarr trie = foldl (\x y -> (insert y x)) trie wordarr

search :: Word -> Trie -> Bool
search [] (Trie end _) = end
search (c:cs) (Trie _ triemap) = fromMaybe False (search cs <$> M.lookup c triemap)


-- AN ATTEMPT AT GETWORDS USING FOLD
{-
getWords :: Trie -> [Word]
getWords trie = foldl1 (\y -> ((prefix [y] trie) ++)) chararr
  where
    chararr = M.keys $ children trie
-}

getWords :: Trie -> [Word]
getWords trie = getWords' trie 0
  where
    chararr = M.keys $ children trie
    getWords' trie n
      | n == length chararr = []
      | otherwise =  (prefix [chararr !! n] trie) ++ getWords' trie (n+1)


prefix :: Word -> Trie -> [Word]
prefix []      (Trie end m) =
    [[] | end] ++ concat [map (c :) (prefix [] tr) | (c, tr) <- M.toList m]
prefix (c : w) (Trie _ m) =
    maybe [] (map (c :) . prefix w) $ M.lookup c m


main = forever $ do
  let trex = empty
  args <- getArgs
  let filename = args !! 0
  --text <- readFile filename
  fileHandle <- openFile filename ReadWriteMode
  text <- hGetContents fileHandle

  let wordlist = words text
  let trie = insertList wordlist trex
  let temptrie = trie
  putStrLn $ "a) Add Word"
  putStrLn $ "s) Search Word"
  putStrLn $ "f) Find words with prefix"
  putStrLn $ "p) Print all words"
  putStrLn $ "e) Exit"
  putStrLn $ "Enter the action:"
  ch <- getLine
  if ch == "a"
    then do putStrLn $ "Enter word/prefix:"
            ch <- getLine
            let trie = insert ch temptrie
            hClose fileHandle
            let newword = "\n" ++ ch
            appendFile filename newword
            print $ "inserted"
    else if ch == "s"
      then do putStrLn $ "Enter word/prefix:"
              ch <- getLine
              print $ search ch trie
      else if ch == "f"
        then do putStrLn $ "Enter word/prefix:"
                ch <- getLine
                print $ prefix ch trie
        else if ch == "p"
          then do print $ getWords trie
                  hClose fileHandle
          else print "error"
  hClose fileHandle

{-
main = do
  args <- getArgs
  let filename = args !! 0
  text <- readFile filename
  let trex = empty
  let wordlist = words text
  let trie = insertList wordlist trex
  print "printing prefix a"
  print $ prefix "a" trie
  print "printing prefix anna"
  print $ prefix "anna" trie
  print "printing search tek"
  print $ search "tek" trie
  print "printing search yahu"
  print $ search "yahu" trie
  print "printing all words"
  print $ getWords trie
  print "inserting kelime into the trie"
  let newtrie = insert "kelime" trie
  print "printing updated all words"
  print $ getWords newtrie
-}
